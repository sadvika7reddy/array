function Map(array, callback) {
    if(array==undefined)
    return [];
    const newArray = [];
    for (let index= 0; index< array.length; index++) {
      newArray.push(callback(array[index], index, array));
    }
    return newArray;
  }
module.exports = Map;