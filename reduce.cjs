function Reduce(array, cb, initialValue) {
    if(array==undefined){
        return [];
    }
    let result;
    if(initialValue==undefined)
     result=array[0]
    else{
      result = initialValue;
    }
    for (let index = initialValue!==undefined?0:1; index< array.length; index+= 1) {
      result = cb(result, array[index], index, array);
    }
    return result;
  }
module.exports = Reduce;