function Filter(array, cb) {
   if(!Array.isArray(array)||array.length==0)
   return [];
    const result = [];
    for (let index= 0; index < array.length; index+= 1) {
      let callback=(cb(array[index],index,array)) 
      if(callback==true){
        result.push(array[index]);
      }
    }
    return result;
  }
  
module.exports = Filter;